## AgentSDK for Laravel

AgentSDK is a tool to connect with Agent Paypment

### Installation

Add IoC from app/start/global.php (Laravel)
~~~php
/**
 * Bind agent SDK to connect agent gateway.
 *
 * @var AgenSDK
 */
App::bind('agentsdk', function($app, $params = array())
{
    $params = array(
        'appId'       => '1',
        'secret'      => 'af87d3615cafe251cc72c8ba357ca6f0',
        'endpoint'    => 'http://agenthost.com/api',
        'redirectUrl' => 'http://agenthost.com/redirect'
    );

    return new AgentSdk\AgentSdk($params);
});
~~~

Standalone application.
~~~php
$params = array(
    'appId'       => '1',
    'secret'      => 'af87d3615cafe251cc72c8ba357ca6f0',
    'endpoint'    => 'http://agenthost.com/api',
    'redirectUrl' => 'http://agenthost.com/redirect'
);

$agentSdk = new AgentSdk\AgentSdk($params);
~~~

## Usage

Generate payment token.

Example:
~~~php

$agentSdk = App::make('agentSdk');

try
{
    $token = $agentSdk->getToken(array(
        'gateway'     => 'paypal',
        'account'     => 'PAYPAL_EMAIL_ACCOUNT',
        'returnUrl'   => 'http://domain.com/checkout/foreground',
        'verifyUrl'   => 'http://domain.com/checkout/background'
        'invoice'     => 'YOUR_INVOICE,
        'description' => 'Buy thing',
        'currency'    => 'THB',
        'language'    => 'EN',
        'amount'      => 100
    ));

    return $this->agentSdk->getRedirectUrl($token);
}
catch (\AgentSdk\AgentException $e)
{
    sd($e->getMessage() . ':' .$e->getDescription(), $e->getErrors());
}
~~~

Process foreground data

~~~php
// Location: http://domain.com/checkout/foreground

$agentSdk = App::make('agentSdk');

$process = $agentSdk->process(Input::all());

var_dump($process->get());
~~~

Process background data.

~~~php
// Location: http://domain.com/checkout/background

$agentSdk = App::make('agentSdk');

$process = $agentSdk->process(Input::all());

$token = $process->get('token');

// OK bill is existing.
if ($process->isSuccess())
{
    // Status from gateway.
    $status = $process->get('status');

    // [DO YOUR LOGIC AROUND HERE]
    // Stamp your order status

    // Checking transaction from a main server.
    if ($this->agentSdk->verify(Input::all()))
    {
        // [DO YOUR LOGIC AROUND HERE]
        // Give user a digital product
        // or push notification.
    }
}

return 'OK';
~~~

TrueMoneyApi need addtion data to request payment token.

~~~php
$token = $this->agentSdk->getToken(array(
    'sandbox'     => true,
    'gateway'     => 'TrueMoneyApi',
    'returnUrl'   => URL::to('demo/background/truemoneyapi'),
    'verifyUrl'   => URL::to('demo/background'),
    'account'     => 'AppId:ShopCode:Secret:Bearer',
    'language'    => 'TH',
    'currency'    => 'THB',
    'invoice'     => uniqid(),
    'description' => 'Pay something',
    'amount'      => 1500,
    'addition'    => array(
        'address' => array(
            'cityDistrict'  => 'Patumwan',
            'companyName'   => 'eCommerce Solution',
            'companyTaxId'  => '3334567',
            'country'       => 'Thailand',
            'email'         => 'me@email.com',
            'forename'      => 'Tee',
            'line1'         => 'Ratchadapisak Rd.',
            'line2'         => 'OX',
            'phone'         => '0867767779',
            'postalCode'    => '10310',
            'stateProvince' => 'Bangkok',
            'surname'       => 'Pluss',
        ),
        'items'   => array(
            array(
                'shopCode'  => 'inherit',
                'itemId'    => 1,
                'service'   => 'bill',
                'productId' => 101,
                'detail'    => 'Something I want to buy',
                'price'     => 1000,
                'ref1'      => 'ref101-1',
                'ref2'      => 'ref101-2',
                'ref3'      => 'ref101-3',
            ),
            array(
                'shopCode'  => 'inherit',
                'itemId'    => 2,
                'service'   => 'bill',
                'productId' => 102,
                'detail'    => 'Something I want to buy',
                'price'     => 500,
                'ref1'      => 'ref102-1',
                'ref2'      => 'ref102-2',
                'ref3'      => 'ref102-3',
            )
        )
    )
));
~~~

TruePaymentApi (WeTrust)
~~~php
$token = $this->agentSdk->getToken(array(
    'sandbox'     => true,
    'gateway'     => 'TruePaymentApi',
    'returnUrl'   => URL::to('demo/background/truemoneyapi'),
    'verifyUrl'   => URL::to('demo/background'),
    'account'     => 'AppId:ShopId:Password:PrivateKey:RC4',
    'language'    => 'TH',
    'currency'    => 'THB',
    'invoice'     => uniqid(),
    'description' => 'Pay something',
    'amount'      => 1,
    'addition'    => array(
        'billing' => array(
            'fullname' => 'Teepluss',
            'address'  => '33/1 Pattanakarn',
            'district' => 'Pattanakarn',
            'province' => 'Bangkok',
            'zip'      => '10220',
            'country'  => 'Thailand'
        ),
        'items'   => array(
            array(
                'pid'         => 18051,
                'productId'   => 'ME161',
                'topic'       => 'Winnie Jewelry : ต่างหูสแควร์ไดมอนด์ (18051)',
                'quantity'    => 1,
                'totalPrice'  => 1,
                'shopIdRef'   => 'inherit',
                'marginPrice' => 0
            ),
            // array(
            //     'pid'         => 18052,
            //     'productId'   => 'ME162',
            //     'topic'       => 'X Winnie Jewelry : ต่างหูสแควร์ไดมอนด์ (18052)',
            //     'quantity'    => 2,
            //     'totalPrice'  => 1,
            //     'shopIdRef'   => 'inherit',
            //     'marginPrice' => 0
            // ),
        )
    )
));
~~~

## Support or Contact

If you have some problem, Contact teepluss@gmail.com


[![Support via PayPal](https://rawgithub.com/chris---/Donation-Badges/master/paypal.jpeg)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=9GEC8J7FAG6JA)
